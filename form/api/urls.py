from django.conf.urls import url, include
from django.contrib import admin

from .views import (
    FielsApiView,
    FieldsDetailAPIView,
)

urlpatterns = [
    url(r'^$', FielsApiView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/$', FieldsDetailAPIView.as_view(), name='detail'),
]
