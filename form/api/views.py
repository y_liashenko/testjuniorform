from rest_framework.generics import ListAPIView, RetrieveAPIView

from form.models import Fields
from .serializers import FormSerializer

class FielsApiView(ListAPIView):
    queryset = Fields.objects.all()
    serializer_class = FormSerializer


class FieldsDetailAPIView(RetrieveAPIView):
    queryset = Fields.objects.all()
    serializer_class = FormSerializer
