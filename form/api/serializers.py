from form.models import Fields
from rest_framework.serializers import ModelSerializer


class FormSerializer(ModelSerializer):
    class Meta:
        model = Fields
        fields = ('id', 'name', 'email', 'phone')