from django.db import models
from django.core.validators import MinLengthValidator

# Create your models here.

class Fields(models.Model):
    name = models.CharField('name', validators=[MinLengthValidator(2)], max_length=50, error_messages={'invalid':"you custom error message"})
    email = models.EmailField('email')
    phone = models.CharField('phone', validators=[MinLengthValidator(5)], max_length=13, null=True, blank=True)

    class Meta:
        verbose_name = 'Form'
        verbose_name_plural = 'Form'

    def __str__(self):
        return self.name
