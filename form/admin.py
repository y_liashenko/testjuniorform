from django.contrib import admin

# Register your models here.
from form.models import Fields

@admin.register(Fields)
class FormFieldsAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'email', 'phone']
