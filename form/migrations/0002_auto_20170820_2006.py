# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-08-20 20:06
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('form', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fields',
            name='name',
            field=models.CharField(error_messages={'invalid': 'you custom error message'}, max_length=50, validators=[django.core.validators.MinLengthValidator(2)], verbose_name='name'),
        ),
        migrations.AlterField(
            model_name='fields',
            name='phone',
            field=models.CharField(max_length=13, validators=[django.core.validators.MinLengthValidator(5)], verbose_name='phone'),
        ),
    ]
