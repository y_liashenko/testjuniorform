from django.forms import ModelForm
from form.models import Fields

class Form(ModelForm):
    class Meta:
        model = Fields
        fields = ('name', 'email', 'phone')
