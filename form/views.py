from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from form.form import *
from .models import Fields

def start(request):
    if request.method == 'POST':
        if request.is_ajax():
            name = request.POST.get('name', None)
            email = request.POST.get('email', None)
            phone = request.POST.get('phone', None)
            data = Fields(
                name=name,
                email=email,
                phone=phone
            )
            data.save()
            return redirect('/')
    return render(request, 'index.html', {
            'form': Form(),
            'content': Fields.objects.all()
        })